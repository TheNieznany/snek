fflags = -fmax-errors=4 -fno-common -fstack-usage
Wflags = -Wall -Wconversion -Wdouble-promotion -Wextra -Wformat=2 -Wformat-overflow -Wformat-truncation -Wshadow -Wstack-usage=8192 -Wundef

release:
	g++ main.cpp -std=c++11 -O2 -lSDL2main -lSDL2 -lSDL2_image -o snek

debug:
	g++ main.cpp -std=c++11 -g $(fflags) $(Wflags) -pedantic -lSDL2main -lSDL2 -lSDL2_image -o test

windows:
	x86_64-w64-mingw32-g++ main.cpp -std=c++11 -O2 -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -o snek.exe
