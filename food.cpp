/*
 * Simple snake game written in C++ with SDL2 library.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of snek.
 *
 * snek is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * snek is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with snek.  If not, see <https://www.gnu.org/licenses/>.
 */

SDL_Point food = {rand() % (WORLD_WIDTH - 1), rand() % (WORLD_HEIGHT - 1)};

void food_draw()
{
	g_fillRect(food.x * TILE_SIZE, food.y * TILE_SIZE,
		TILE_SIZE, TILE_SIZE, COLOR_RED);
}

void food_update()
{
	if (food.x == snake.front().x && food.y == snake.front().y)
	{
			spawn:

			food.x = rand() % (WORLD_WIDTH - 1);
			food.y = rand() % (WORLD_HEIGHT - 1);

			for (const auto& s : snake)
			{
				if (food.x == s.x && food.y == s.y) goto spawn;
			}

			snake.push_back({snake.back().x, snake.back().y});
	}
}
