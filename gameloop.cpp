/*
 * Simple snake game written in C++ with SDL2 library.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of snek.
 *
 * snek is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * snek is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with snek.  If not, see <https://www.gnu.org/licenses/>.
 */

void game_update()
{
	snake_update();
	food_update();
}

void game_draw()
{
	g_setColor(COLOR_BLACK);
	SDL_RenderClear(g_renderer);

	snake_draw();
	food_draw();

	g_draw();
}

void game_handleInput()
{
	if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
	{
		running = false;
	}

	/* ----- CONTROLS ----- */
	if (i_keys[SDL_SCANCODE_ESCAPE]) running = false;

	if (i_keys[SDL_SCANCODE_UP])		new_dir = UP;
	if (i_keys[SDL_SCANCODE_RIGHT])		new_dir = RIGHT;
	if (i_keys[SDL_SCANCODE_DOWN])		new_dir = DOWN;
	if (i_keys[SDL_SCANCODE_LEFT])		new_dir = LEFT;
}

void game_loop()
{
	uint32_t acc = 0;

	uint32_t frameStart,
			 frameEnd,
			 deltaTime = 0;

	while (running)
	{
		if (deltaTime < 1)
		{
			frameStart = SDL_GetTicks();
			SDL_Delay(1);
			frameEnd = SDL_GetTicks();
			deltaTime = frameEnd - frameStart;
		}

		frameStart = SDL_GetTicks();

		game_handleInput();

		acc += deltaTime;

		if (acc >= GAME_SPEED)
		{
			game_update();
			game_draw();
			acc -= GAME_SPEED;
		}

		frameEnd = SDL_GetTicks();
		deltaTime = frameEnd - frameStart;
	}
}
