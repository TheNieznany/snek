/*
 * Simple snake game written in C++ with SDL2 library.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of snek.
 *
 * snek is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * snek is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with snek.  If not, see <https://www.gnu.org/licenses/>.
 */

SDL_Window* g_window;
SDL_Renderer* g_renderer;

SDL_Texture* g_mainTexture;



void g_draw()
{
	SDL_SetRenderTarget(g_renderer, NULL);
	SDL_RenderCopy(g_renderer, g_mainTexture, NULL, NULL);
	SDL_RenderPresent(g_renderer);
	SDL_SetRenderTarget(g_renderer, g_mainTexture);
}



inline void g_setColor(const SDL_Color& _color = COLOR_DEFAULT_BG)
{
	SDL_SetRenderDrawColor(g_renderer, _color.r, _color.g, _color.b, _color.a);
}



void g_drawPoint(int _x, int _y, const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);
	SDL_RenderDrawPoint(g_renderer, _x, _y);
}

void g_drawLine(int _x1, int _y1, int _x2, int _y2,
	const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);
	SDL_RenderDrawLine(g_renderer, _x1, _y1, _x2, _y2);
}

void g_drawRect(int _x, int _y, int _w, int _h,
	const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);

	SDL_Rect rect = {_x, _y, _w, _h};
	SDL_RenderDrawRect(g_renderer, &rect);
}

void g_drawRect2(const SDL_Rect& _rect,
	const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);
	SDL_RenderDrawRect(g_renderer, &_rect);
}

void g_fillRect(int _x, int _y, int _w, int _h,
	const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);

	SDL_Rect rect = {_x, _y, _w, _h};
	SDL_RenderFillRect(g_renderer, &rect);
}

void g_fillRect2(const SDL_Rect& _rect,
	const SDL_Color& _color = COLOR_DEFAULT_FG)
{
	g_setColor(_color);
	SDL_RenderFillRect(g_renderer, &_rect);
}





void g_init()
{
	g_window = SDL_CreateWindow(
		SCREEN_TITLE,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		0
	);

	g_renderer = SDL_CreateRenderer(
		g_window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE
	);

	g_mainTexture = SDL_CreateTexture(
		g_renderer,
		SDL_PIXELFORMAT_UNKNOWN,
		SDL_TEXTUREACCESS_TARGET,
		MAIN_WIDTH,
		MAIN_HEIGHT
	);

	SDL_SetRenderTarget(g_renderer, g_mainTexture);

	IMG_Init(IMG_INIT_PNG);
}

void g_quit()
{
	IMG_Quit();

	SDL_DestroyTexture(g_mainTexture);

	SDL_DestroyRenderer(g_renderer);
	SDL_DestroyWindow(g_window);
}
