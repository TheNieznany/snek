/*
 * Simple snake game written in C++ with SDL2 library.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of snek.
 *
 * snek is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * snek is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with snek.  If not, see <https://www.gnu.org/licenses/>.
 */

/* ===== ----- INCLUDES ----- ===== */

#include <stdlib.h>
#include <time.h>

#include <list>
using std::list;

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>



/* ===== ----- DEFINES ----- ===== */

#define SCREEN_WIDTH			320
#define SCREEN_HEIGHT			240
#define SCREEN_TITLE			"snek"

#define MAIN_WIDTH				160
#define MAIN_HEIGHT				120

#define WORLD_WIDTH				20
#define WORLD_HEIGHT			15

#define TILE_SIZE				8

#define COLOR_WHITE				{0xFF, 0xFF, 0xFF, 0xFF}
#define COLOR_BLACK				{0x00, 0x00, 0x00, 0xFF}
#define COLOR_RED				{0xFF, 0x00, 0x00, 0xFF}
#define COLOR_GREEN				{0x00, 0xFF, 0x00, 0xFF}
#define COLOR_BLUE				{0x00, 0x00, 0xFF, 0xFF}

#define COLOR_DEFAULT_FG		COLOR_WHITE
#define COLOR_DEFAULT_BG		COLOR_BLACK

#define GAME_SPEED				250


/* ===== ----- VARIABLES ----- ===== */

SDL_Event event;
const uint8_t* i_keys;
bool running = true;



/* ===== ----- LOCAL INCLUDES ----- ===== */

#include "graphics.cpp"

#include "snake.cpp"
#include "food.cpp"

#include "gameloop.cpp"



/* ===== ----- MAIN FUNCTION ----- ===== */

int main(int argc, char** argv)
{
	srand(time(NULL));

	i_keys = SDL_GetKeyboardState(NULL);

	SDL_Init(SDL_INIT_VIDEO);
	g_init();

	game_loop();

	g_quit();
	SDL_Quit();
	return 0;
}
