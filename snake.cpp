/*
 * Simple snake game written in C++ with SDL2 library.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of snek.
 *
 * snek is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * snek is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with snek.  If not, see <https://www.gnu.org/licenses/>.
 */

list<SDL_Point> snake = {{4, 2}, {3, 2}, {2, 2}};

enum Direction
{
	UP,
	RIGHT,
	DOWN,
	LEFT
};
Direction snake_dir = RIGHT;
Direction new_dir = RIGHT;

void snake_draw()
{
	for (const auto& s : snake)
	{
		g_fillRect(s.x * TILE_SIZE, s.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
	}

	g_fillRect(snake.front().x * TILE_SIZE, snake.front().y * TILE_SIZE,
		TILE_SIZE, TILE_SIZE, COLOR_GREEN);
}

void snake_update()
{
	switch (new_dir)
	{
	case UP:
		if (snake_dir != DOWN) snake_dir = new_dir;
		break;

	case RIGHT:
		if (snake_dir != LEFT) snake_dir = new_dir;
		break;

	case DOWN:
		if (snake_dir != UP) snake_dir = new_dir;
		break;

	case LEFT:
		if (snake_dir != RIGHT) snake_dir = new_dir;
		break;
	}



	switch (snake_dir)
	{
	case UP:
		if (snake.front().y == 0)
		{
			snake.push_front({snake.front().x, (WORLD_HEIGHT - 1)});
		}
		else
		{
			snake.push_front({snake.front().x, snake.front().y - 1});
		}
		snake.pop_back();
		break;

	case RIGHT:
		if (snake.front().x == (WORLD_WIDTH - 1))
		{
			snake.push_front({0, snake.front().y});
		}
		else
		{
			snake.push_front({snake.front().x + 1, snake.front().y});
		}
		snake.pop_back();
		break;

	case DOWN:
		if (snake.front().y == (WORLD_HEIGHT - 1))
		{
			snake.push_front({snake.front().x, 0});
		}
		else
		{
			snake.push_front({snake.front().x, snake.front().y + 1});
		}
		snake.pop_back();
		break;

	case LEFT:
		if (snake.front().x == 0)
		{
			snake.push_front({(WORLD_WIDTH - 1), snake.front().y});
		}
		else
		{
			snake.push_front({snake.front().x - 1, snake.front().y});
		}
		snake.pop_back();
		break;
	}

	bool first = true;

	for (const auto& s : snake)
	{
		if (first)
		{
			first = false;
			continue;
		}
		if (snake.front().x == s.x && snake.front().y == s.y)
		{
			running = false;
		}
	}
}
